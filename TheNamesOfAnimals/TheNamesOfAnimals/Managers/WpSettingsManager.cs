﻿using System;
using System.Diagnostics;
using System.Globalization;
using Windows.Graphics.Display;
using Windows.Storage;
using Windows.UI.Xaml;
using TheNamesOfAnimals.Managers;

namespace TheNamesOfAnimals.WinPhone.Managers
{
    public class WpSettingsManager : SettingsManager
    {
        public override void Remover()
        {
            ApplicationData.Current.LocalSettings.Values.Remove(CounterInitKey);
            ApplicationData.Current.LocalSettings.Values.Remove(isRantedKey);
        }
       
        public override void SaveInt(string key, int value)
        {
            ApplicationData.Current.LocalSettings.Values[key] = value; 
        }

        public override int? GetInt(string key)
        {
            return ApplicationData.Current.LocalSettings.Values[key] as int?;
        }

        public override void SaveBool(string key, bool value)
        {
            ApplicationData.Current.LocalSettings.Values[key] = value;
        }

        public override bool? GetBool(string key)
        {
            return ApplicationData.Current.LocalSettings.Values[key] as bool?;
        }

        public override double Resolution()
        {
            double ScaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            double resolution = ScaleFactor*Window.Current.Bounds.Width;
            return resolution;
           
        }


    }
}
