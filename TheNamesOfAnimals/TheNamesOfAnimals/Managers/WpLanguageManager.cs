﻿using System;
using System.Diagnostics;
using System.Globalization;
using Windows.Storage;
using TheNamesOfAnimals.Managers;

namespace TheNamesOfAnimals.WinPhone.Managers
{
    public class WpLanguageManager : LanguageManager
    {


        public override string Language { get; set; }

        public override void SaveString(string key, string value)
        {

            ApplicationData.Current.LocalSettings.Values[key] = value;

        }

        public override string GetString(string key)
        {
            Language = ApplicationData.Current.LocalSettings.Values[key] as string;
            return Language;
        }

        public override string CheckLanguage()
        {
            Language = GetString("lang");
            if (Language != null)
            {
                
                Debug.WriteLine("Выбран язык из настроек приложения: {0}", Language);
                return Language;
            }
            else
            {
                CultureInfo ci = new CultureInfo(Windows.System.UserProfile.GlobalizationPreferences.Languages[0]);
                string _key = Convert.ToString(ci).Substring(0, 2);
                Debug.WriteLine("Выбран язык из настроек телефона: {0}", _key);
                if (_key != "en" && _key != "fr" && _key != "it" && _key != "ru")
                    _key = "en";
                
                Language = _key.ToUpper();//ebanyj v rot blyad. tozhe samoe tut popravish. papki UPPERCASE, a tut vse lowercase
                SaveString("lang",Language);
                
                return Language;
                

            }



        }

        
    }
}