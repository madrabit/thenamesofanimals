﻿using System;
using System.Diagnostics;
using System.Globalization;
using TheNamesOfAnimals.Managers;
using Windows.ApplicationModel.Store;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using TheNamesOfAnimals.ViewModels;
using Store = Windows.ApplicationModel.Store;
using System.Windows;
using Windows.Foundation;
using Windows.Graphics.Display;
using Windows.Security.Cryptography.Core;


// под дебагомсе мы подменяем API на фейковоес 
// эта хуйнря нахывается условная компиляция


#if DEBUG
using CurrentApp = Windows.ApplicationModel.Store.CurrentAppSimulator;
#else
using CurrentApp = Windows.ApplicationModel.Store.CurrentApp;
#endif


namespace TheNamesOfAnimals.WinPhone.Managers
{
    public class WpStoreManager : StoreManager
    {
        //Rect bounds = Window.Current.Bounds;
        //DisplayInformation displayInfo = DisplayInformation.GetForCurrentView();



        string _purchase = "Purchase is complete";
        string _close = "Close";
        string _comment = "";
        string _leavComment = "";
        string _buy = "";
        string _accessFull = "";


        public override void Vocabulary()
        {
            string chosenLanguage = LanguageManager.Instance.CheckLanguage();

            if (chosenLanguage != null)
            {
                switch (chosenLanguage)
                {
                    case "EN":
                        _purchase = "Purchase is complete";
                        _close = "Close";
                        _comment = "If you like the game, please leave a review";
                        _leavComment = "Write a review";
                        _buy = "Buy";
                        break;
                    case "FR":
                        _purchase = "Vous avez achete";
                        _close = "Clore";
                        _comment = "Si vous aimez le jeu, laissez s'il vous plaît un avis";
                        _leavComment = "Laisser un commentaire";
                        _buy = "Аcheter";
                        break;
                    case "IT":
                        _purchase = "Hai acquistato";
                        _close = "Fermer";
                        _comment = "Se ti piace il gioco, si prega di lasciare un commento";
                        _leavComment = "Lascia un commento";
                        _buy = "Аcquistare";
                        break;
                    case "RU":
                        _purchase = "Покупка совершена";
                        _close = "Закрыть";
                        _comment = "Если Вам понравилась игра, оставьте, пожалуйста, отзыв";
                        _leavComment = "Оставить отзыв";
                        _buy = "Купить";
                        _accessFull = "Альбом доступен в полной версии приложения";
                        break;
                }


            }
        }


        public void SaveBool(string key, bool value)
        {
            ApplicationData.Current.LocalSettings.Values[key] = value;
        }

        public static bool? GetBool(string key)
        {
            return ApplicationData.Current.LocalSettings.Values[key] as bool?;
        }



        //в двух словах нахуй тут асинх и че он творит
        // там всё апи асинхронное. без асинка нельзя ok
        // ну тогда пока вроде все надо провыткать код
        // чтобы сбросить покупку в твоём приложении типа "куплено" надо удалить приложение с телефона. омг а оно не перебилдится?
        // если ребилд сделать то наверно само переустановит (а не простой билд) ок 
        // ты понял что тебе при деплое в магазин приложений надо будет release схему поставить вместо дебага? угу
        //public override async void Donate(object sender, EventArgs e)


        //readonly bool _purchase = false;


        public override async void Donate()
        {


            // этот кусок будет выполняться только под дебагом
            // он инициализирует фейковый магазин лол пашет
            // хм. даже настройка не нужна наверно будет. Просто брать из CurrentApp.LicenseInformation.ProductLicenses[FullVersionInAppCode].IsActive
            // и норм. huy znaet. on kazhdyj raz zhe gruzit infu iz etogo faila WindowsStoreFakeData
            // a on ne obnovlyaetsya ну симулятор ладно, а реальный магаз че?
            // realnyj dolzhen gruzit'
            // ну тогда болт. vse?ща покажу кое че


#if DEBUG
            Uri uri = new Uri("ms-appx:///WindowsStoreFakeData.xml");
            StorageFile storeProxy = await StorageFile.GetFileFromApplicationUriAsync(uri);
            await CurrentAppSimulator.ReloadSimulatorAsync(storeProxy);
#endif
            //string purchase = "purchase";










            if (!CurrentApp.LicenseInformation.ProductLicenses[FullVersionInAppCode].IsActive)
            {
                
                try
                {
                    PurchaseResults purchaseResult = await CurrentApp.RequestProductPurchaseAsync(FullVersionInAppCode);
                    if (purchaseResult.Status == ProductPurchaseStatus.Succeeded)
                    {
                        Debug.WriteLine("kupleno yoba");

                        MessageDialog msg = new MessageDialog(_purchase);

                        msg.Commands.Add(new UICommand("Ok"));



                        //msg.Commands.Add(new UICommand("Закрыть"));
                        // che ne rabotaet. a ona i ne dolzhna byla rabotat
                        // мбля просто работала до этого
                        // а когда реальный магаз включен, она уже навсегда запомнится в магазе?
                        // покупка проходит. вылетает диалог, ну чтоб проверить. выхожу из приложения. покупка слетает
                        await msg.ShowAsync();

                        //СОХРАНЯЕМ ФАКТ ПОКУПКИ ЛОКАЛЬНО
                        SaveBool("purchase", true);



                        // аттут и ниже чет надо? заивисит от того что ты тут хочешь. может показать картинку "ебать ты лох"
                        // а ты показывал. вы купили бог.
                        // ну я показывал. / 
                        // самое простое  меседж бокс, ну или сразу в UI ты как то отобразишь
                        // ещё чет посмотреть нада или че

                        ////  яс амжое  помгу

                        // продукт куплен повесстить юзера об этом
                    }
                    else
                    {
                        Debug.WriteLine("ne kupleno yoba");
                        // продукт не был куплен
                        SaveBool("purchase", false);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ne kupleno yoba (exception)");
                    SaveBool("purchase", false);
                    // Обрабатываем ошибки куда смотреть йоба
                }
            }
            else
            {
                Debug.WriteLine("uzhe kupleno yoba");
                SaveBool("purchase", true);


                //"Сообщаем пользователю о том, что он уже купил продукт."
            }
        }
        // вот эту строку я плохо воткнул. это ж проверятель при заходе в приложение куплена али нет.

        //public override bool FullVersion => CurrentApp.LicenseInformation.ProductLicenses[FullVersionInAppCode].IsActive;

        

        private bool PurchaseChecker
        {
            get

            {
                var b = GetBool("purchase");
                return b != null && (bool)b;

            }
            set { value = false; }
        } 

        public override bool FullVersion => PurchaseChecker;

        // это аналогично эьому: а и она отдаст тру фолс. и ну там само дальше
        // да, тру отдаст если куплено.







        //Вызов рейтинга
        async public override void RankMethod()
        {
            await Windows.System.Launcher.LaunchUriAsync(
                  new Uri("ms-windows-store:reviewapp?appid=" + CurrentApp.AppId));
        }

        
        
        //Диалоговое окно рейтинга
        public async override void RankEvent()
        {
           
            MessageDialog msg = new MessageDialog(_comment);

            
            msg.Commands.Add(new UICommand(_leavComment, new UICommandInvokedHandler(CommandHandlersOk)));
            msg.Commands.Add(new UICommand(_close, new UICommandInvokedHandler(CommandHandlersCancel)));

            await msg.ShowAsync();
           
        }

        public async override void PaymentReminder()
        {
            MessageDialog msg = new MessageDialog(_accessFull);


            msg.Commands.Add(new UICommand(_buy, new UICommandInvokedHandler(CommandHandlersGoToStore)));
            msg.Commands.Add(new UICommand(_close, new UICommandInvokedHandler(CommandHandlersCancel)));

            await msg.ShowAsync();
        }

        public void CommandHandlersGoToStore(IUICommand commandLabel)
        {

           Donate();

        }

        public void CommandHandlersCancel(IUICommand commandLabel)
        {
            
            SettingsManager.Instance.SaveBool(SettingsManager.isRantedKey, false);
            SettingsManager.Instance.SaveInt(SettingsManager.CounterInitKey, 0);

        }


        public void CommandHandlersOk(IUICommand commandLabel)
        {
            SettingsManager.Instance.SaveBool("IsRated", true);
            RankMethod();
            

        }



    }
}




//public override async void Donate()
//{
//    if (!Store.CurrentApp.LicenseInformation.ProductLicenses["TheNameOfAnimals"].IsActive)
//    {
//        try
//        {
//            await CurrentApp.RequestProductPurchaseAsync("TheNameOfAnimals", false);
//            ProductLicense productLicense = null;
//            if (CurrentApp.LicenseInformation.ProductLicenses.TryGetValue("TheNameOfAnimals", out productLicense))
//            {
//                if (productLicense.IsActive)
//                {
//                    //("Сообщение о том как мы благодарны за покупку ;)"

//                    // Сообщаем Магазину, о том что продукт доставлен
//                    CurrentApp.ReportProductFulfillment("TheNameOfAnimals");
//                    return;
//                }
//            }
//        }
//        catch (Exception ex)
//        {
//            // Обрабатываем ошибки куда смотреть йоба
//        }
//    }
//    else
//    {
//        //"Сообщаем пользователю о том, что он уже купил продукт."

//    }



//}

/*
public static bool _fullversion { get; set; }
public override bool  FullVersion {
get { return _fullversion; }


set
{
    if (CurrentApp.LicenseInformation.ProductLicenses["TheNameOfAnimals"].IsActive)
    {
        _fullversion = true;
    }
    else
    {
        _fullversion = false;
    }

}


}
*/
