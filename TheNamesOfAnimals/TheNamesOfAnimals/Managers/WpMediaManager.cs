﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using TheNamesOfAnimals.Managers;


namespace TheNamesOfAnimals.WinPhone.Managers
{
    public class WpMediaManager : MediaManager
    {

//        public string Language
//        {
//            get
//            {
//                CultureInfo ci = new CultureInfo(Windows.System.UserProfile.GlobalizationPreferences.Languages[0]);
//                string lang = Convert.ToString(ci).ToUpper().Substring(0, 2);
//                return lang;

////                System.Diagnostics.Debug.WriteLine("CI ЕБУЧИЙ " + str +
////    "*************************************   CultureInfo.CurrentCulture.Name = " + CultureInfo.CurrentCulture.ToString() + ", " +
////    "CultureInfo.CurrentCulture.CompareInfo = " + CultureInfo.CurrentCulture.CompareInfo + ", " +
////    "CultureInfo.CurrentCulture.DisplayName = " + CultureInfo.CurrentCulture.DisplayName + ", " +
////    "CultureInfo.CurrentCulture.EnglishName = " + CultureInfo.CurrentCulture.EnglishName + ", " +
////    "CultureInfo.CurrentCulture.Name = " + CultureInfo.CurrentCulture.Name + ", " +
////    "CultureInfo.CurrentCulture.NativeName = " + CultureInfo.CurrentCulture.NativeName + ", " +
////    "CultureInfo.CurrentCulture.TextInfo = " + CultureInfo.CurrentCulture.TextInfo
////);
//            }
//        }

        private MediaElement MusicPlayer { get; set; }
        private MediaElement SoundPlayer { get; set; }


        private bool _isInitialized;
        private bool _isPlayingMusic;

        private static readonly object InitializeLocker = new object();
        private static readonly object MusicLocker = new object();
        private static readonly object SoundLocker = new object();

        // создали базу для того чтобы конкретно дёргать наши медиаэлементы

        public override void Initialize()
        {
            // у нас тут опасный код, поэтому в лок пихаем
            // а если не пихать. че будет например? ну мы тут инициализируем
            // этот код будет выполняться при загрузке каждой страницы
            // поэтому если на 1 ты начал инициализировать, перешел на вторую
            // а на второй начинаешь опять инициализировать, а тебе уже не надо.
            // поэтому ставим лок, чтобы на 2 странице мы не делали того что ниже написано,
            // пока 1 страница не закончит это делать jok
            lock (InitializeLocker)
            {
                // если уже проинициализировали всё, не делаем этого больше
                if (_isInitialized)
                    return;
                if (Window.Current == null || Window.Current.Content == null)
                    return;

                // получили грид из Styles.xaml RootFrameStyle
                DependencyObject rootGrid = VisualTreeHelper.GetChild(Window.Current.Content, 0);

                if (rootGrid == null)
                    return;

                // получаем медиэлементы этого грида по порядку 
                // мы конечно попробуем. но всё может пиздой накрыться. но похуй. да я так делал уже. ниче не изменило
                SoundPlayer = (MediaElement)VisualTreeHelper.GetChild(rootGrid, 0);
                MusicPlayer = (MediaElement)VisualTreeHelper.GetChild(rootGrid, 1);
     

                // короче запусти и полистай до момента когда перестанет звук воспроизводиться

                // мы получили медиаэлементы. Теперь можно делать что хочешь с ними

                _isInitialized = true;
            }
        }


        // у нас базовый класс абстракнтый. когда ты унаследуешься от него,
        // у абстрактного класса если абстрактные методы.,которые ты должен реализовать еу. 

        // ВОПРОС. Почему два метода PlaySound называются. Как один может в другом работать? 
        // И нельзя чтобы один был?

        // у тебя PlaySound от PlayVoice отличаются только папкой, где хранится звук.
        // тот же самый код плодить в двух методах отличающихся только папкой нехорошо,
        // поэтому я сделал общий метод PlaySound(string code, string soundFolder, Action callback = null)
        // в который передаю из какой папки звук проигрывать
        // а наружу пустил два метода без этой папки, т.к. снаружи об этом не обязательно никому знать
        // понял


        
        public override void PlaySound(string code, Action callback = null)
        {
            PlaySound(code, "Sounds", callback);
        }
    
        public override void PlayVoice(string code, Action callback = null)
        {

            PlaySound(code, string.Format("Voice/{0}", LanguageManager.Instance.GetString("lang")), callback);
            
        }



        public override void PlayAnswer(string code, Action callback = null)
        {
            PlaySound(code, string.Format("Voice/{0}", LanguageManager.Instance.GetString("lang")), callback);
        }
        //public override void PlayAnswer(string code)
        //{
        //    try
        //    {
        //        lock (SoundLocker)
        //        {
        //            if (SoundPlayer == null)
        //                return;
                    
                    
        //            SoundPlayer.Source = new Uri(string.Format("ms-appx:///Assets/Voice/{0}.wav", code));
        //        }
        //    }
        //    catch
        //    {
        //        // не дай бог
        //    }
        //}


        // gemor nahuy
        // tut nado pisat 4toby neslkolko podryad vosproizvelos
        // i 4toby mozhno bylo ostanavlivat. shya neoxota dumat ок

            /*
        public void PlaySoundsOneByOne(List<Sound> sounds) vse? угу
        {
            ...
        }
        */

        public void PlaySound(string code, string soundFolder, Action callback = null)
        {
            try
            {
                Debug.WriteLine("Проигрываем звук {0}.wav из папки {1}, колбек стоит: {2}", code, soundFolder, callback != null);

                lock (SoundLocker)
                {
                    if (SoundPlayer == null)
                        return;

                    // у MediaElement'ов мы проставили AutoPlay = true
                    // это значит, что как только ты выставил источник звуков, плеер автоматом начинает 
                    // воспроизведение
                    // тут ms-appx:/// - корень . Добавишь музыку куда хочешь.
                    // тут формируется путь до музыкального файла в зависимости от какого-то кода ok

                    // callback - действие, которое надо будет выполнить потом после того,
                    // как отыграет звук
                   
                 
                    if (callback != null)
                    {
                        // создаём обработчик на событие MediaEnded

                        // НО. мы подписались бы и не отписались
                        // то есть подписывалаись бы каждый раз и засрались
                        // да. поэтому такая уловка.
                        // ну все вроде более менее понял о_О ы_ы
                        
                        RoutedEventHandler mediaEndedDelegate = null;
                        mediaEndedDelegate = delegate (object sender, RoutedEventArgs args)
                        {
                            try
                            {
                                // если остановили вручную, то не проигрываем следующий звук
                                // ВОПРОС как может быть ручная остановка? о чем ваще речь?
                                // отписываемся от события MediaEnded, чтобы не жралась память
                                // это событие, когда закончился wav файл твой

                                SoundPlayer.MediaEnded -= mediaEndedDelegate;
                                // в котором вызываем callback -действие, которое мы должны выполнить после
                                // завершения проигрыша
                                //ВОПРОС  что в данном случае делает callback();? проигрывает звук?
                                // ваще не оч понимаю штуку эту колбек, что жэто такое

                                // callback - это тот метод, который ты во вьюмодели передал
                                // вот когда ты вызовешь тут callback()
                                // 
                                callback();
                            }
                            catch
                            {
                                // ВОПРОС почему все catch пустые?
                                // кэчти нахуй не нужны?
                                // пустые кетчи это плохо, но иногда лучше,
                                // чтобы он просто проглотил тут исключение (не факт что оно вообще будет)
                                // чем упал
                                // особенно если ошибка не критичная (не проиграется звук и хер бы сним)
                                // ты же не будешь ребёнку ошибку показывать.
                                // или приложение чтобы падало сразу. короче чтобы не вырубало полностью приложуху

                                // не дай бог
                            }
                        };
                        // работает. а че ты убрал оО ну флажок. я убрал остановку при переключении.
                        // он сам должен заменять текущий звук на новый, это не надо.
                        // а там остановку он делал после того как новый шел. и тот баг пропал, когда при первом нажатии только голос был
                        // работает вроде усё
                        // подписываемся на собатие MediaEnded - т.е. когда закончился звук
                        // ВОПРОС что происходит когда мы тут подписались на событие? файл есть при нажатии голос работает.
                        SoundPlayer.MediaEnded += mediaEndedDelegate;

                    }

                  //  Debug.WriteLine(string.Format("[at {0}] ms-appx:///assets/{1}/{2}.wav", DateTime.Now, soundFolder, code));
                    SoundPlayer.Source = new Uri(string.Format("ms-appx:///Assets/{0}/{1}.wav", soundFolder, code));
                }
            }
            catch
            {
                // не дай бог
            }
        }

        public void PlayAnswerAndTask(string code, string soundFolder,  Action callback = null, Action callback2 = null)
        {
            try
            {
                if (SoundPlayer == null)
                    return;



                SoundPlayer.Source = new Uri(string.Format("ms-appx:///Assets/Voice/correctly.wav"));

                lock (SoundLocker)
                {
                  
                    if (SoundPlayer == null)
                        return;



                    if (callback != null)
                    {
                        

                        RoutedEventHandler mediaEndedDelegate = null;
                        mediaEndedDelegate = delegate (object sender, RoutedEventArgs args)
                        {
                            try
                            {
                               

                                SoundPlayer.MediaEnded -= mediaEndedDelegate;
                               
                               
                                callback(); 

                            }
                            catch
                            {
                                //
                            }
                        };
                        
                        SoundPlayer.MediaEnded += mediaEndedDelegate;

                    }

                    SoundPlayer.Source = new Uri(string.Format("ms-appx:///assets/{0}/{1}.wav", soundFolder, code));

                }
            }
            catch
            {
                // 
            }
        }
        public override void StopSound()
        {
            try
            {
                lock (SoundLocker)
                {
                    if (SoundPlayer == null)
                        return;

                    if (SoundPlayer.CurrentState == MediaElementState.Playing)
                    {
                        SoundPlayer.Stop();
                    }
                }
            }
            catch
            {
                //ignored
            }
        }

        public override void PlayMusic(string code)
        {
            Debug.WriteLine("Вход в PlayMusic {0}", code);
            // если музыкальный медиаэлемент не инициализирован или если уже играет музыка, то уходим
            if (MusicPlayer == null || _isPlayingMusic)
                return;

            // на всякий случай пихаем в лок, чтобы не начать воспроизводить музыку заново,
            // когда она уже играет
            lock (MusicLocker)
            {
                Debug.WriteLine("Проигрываем музыку {0}", code);

                MusicPlayer.Source = new Uri(string.Format("ms-appx:///Assets/Music/{0}.mp3",code));
                Debug.WriteLine("Проигрываем музыку {0}", MusicPlayer.Source);
                _isPlayingMusic = true;
            }
        }


        public override void StopMusic()
        {
            lock (MusicLocker)
            {
                if (_isPlayingMusic)
                {
                    MusicPlayer.Stop();
                    _isPlayingMusic = false;
                }
            }
        }
    }
}
