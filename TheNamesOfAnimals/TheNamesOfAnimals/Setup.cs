using System.Reflection;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.WindowsCommon.Platform;
using Windows.UI.Xaml.Controls;
using TheNamesOfAnimals.ViewModels;

namespace TheNamesOfAnimals.WinPhone
{
    public class Setup : MvxWindowsSetup
    {
        public Setup(Frame rootFrame) : base(rootFrame)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new WpApp();
        }
		
        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override Assembly[] GetViewModelAssemblies()
        {
            return new[] {typeof (MenuViewModel).GetTypeInfo().Assembly};
        }
    }
}