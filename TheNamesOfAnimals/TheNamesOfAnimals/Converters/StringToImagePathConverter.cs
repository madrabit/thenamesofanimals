﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;
using TheNamesOfAnimals.WinPhone.Managers;

namespace TheNamesOfAnimals.WinPhone.Converters
{

    public class StringToImagePathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {

            //if (TheNamesOfAnimals.Managers.SettingsManager.Instance.ScaleFactor() < 2)
            //{
            //    Uri imageUri = new Uri(string.Format("ms-appx:///Assets/Smallimg/{0}.png", value));
            //    BitmapImage bitmapImage = new BitmapImage(imageUri);
            //    return bitmapImage;
            //}
            //else
            //{
                Uri imageUri = new Uri(string.Format("ms-appx:///Assets/{0}.png", value));
                BitmapImage bitmapImage = new BitmapImage(imageUri);
                return bitmapImage;
            //}
            

            
            // короче. конвертеры можно перенести в отдельный xaml!

            
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {

            throw new NotImplementedException();
        }
    }

   
}
