﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;
using TheNamesOfAnimals.Managers;
using TheNamesOfAnimals.WinPhone.Managers;


namespace TheNamesOfAnimals.WinPhone.Converters
{
    

    public class BoolStoreVisibilityConverter : IValueConverter
    {
        

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            //if (targetType != typeof(Visibility))
            //    throw new InvalidOperationException("The target must be a Visibility.");

            //bValue.HasValue &&


            bool? bValue = (bool?)value;

            //  не куплено
            
            //основная строка, которая останется
            if (StoreManager.Instance.FullVersion == false)
            {
                return bValue.Value ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                return bValue.Value ? Visibility.Collapsed : Visibility.Visible;
            }
          


            // куплено
          // 
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {

            throw new NotImplementedException();
        }
    }
}