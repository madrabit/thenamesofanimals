﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;
using TheNamesOfAnimals.WinPhone.Managers;

namespace TheNamesOfAnimals.WinPhone.Converters
{
    public class StringToImageAnimalConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (TheNamesOfAnimals.Managers.SettingsManager.Instance.Resolution() < 1280)
            {
                Uri imageUri = new Uri(string.Format("ms-appx:///Assets/Smallimg/{0}.jpg", value));
                BitmapImage bitmapImage = new BitmapImage(imageUri);
                return bitmapImage;
                Debug.WriteLine("Resolution = {0}", TheNamesOfAnimals.Managers.SettingsManager.Instance.Resolution());
            }
            else
            {
                Uri imageUri = new Uri(string.Format("ms-appx:///Assets/Animals/{0}.jpg", value));

                BitmapImage bitmapImage = new BitmapImage(imageUri);
                Debug.WriteLine("Resolution = {0}", TheNamesOfAnimals.Managers.SettingsManager.Instance.Resolution());


                return bitmapImage;
            }
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {

            throw new NotImplementedException();
        }
    }
}
