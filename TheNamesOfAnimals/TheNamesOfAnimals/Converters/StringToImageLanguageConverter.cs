﻿using System;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TheNamesOfAnimals.WinPhone.Converters
{
    public class StringToImageLanguageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            Uri imageUri = new Uri(string.Format("ms-appx:///Assets/Buttons/{0}.png", value));

            BitmapImage bitmapImage = new BitmapImage(imageUri);


            return bitmapImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {

            throw new NotImplementedException();
        }
    }
}
