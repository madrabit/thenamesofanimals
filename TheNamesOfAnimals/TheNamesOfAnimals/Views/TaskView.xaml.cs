﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TheNamesOfAnimals.Managers;
using TheNamesOfAnimals.ViewModels;



namespace TheNamesOfAnimals.WinPhone.Views
{
    
    public sealed partial class TaskView 
    {
        public TaskView()
        {
            InitializeComponent();
            Loaded += OnLoaded;
            Unloaded += OnUnloaded;
        }
        private object _lock = new object();
        private bool _controlLoaded = false;

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            lock (_lock)
            {
               
                MediaManager.Instance.Initialize();
                
                TaskViewModel taskViewModel = DataContext as TaskViewModel;
                if (taskViewModel != null)
                {
                    taskViewModel.PlayTaskSound();
                }

                _controlLoaded = true;
            }
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            
            try
            {
                Loaded -= OnLoaded;
                Unloaded -= OnUnloaded;
                MediaManager.Instance.StopSound();
            }
            catch
            {
                //ignored
            }
        }

    }
}
