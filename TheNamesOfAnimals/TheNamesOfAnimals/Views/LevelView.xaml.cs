﻿
using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using TheNamesOfAnimals.Entities;
using TheNamesOfAnimals.Managers;
using TheNamesOfAnimals.ViewModels;

namespace TheNamesOfAnimals.WinPhone.Views
{
    public sealed partial class LevelView
    {
        public LevelView()
        {
            InitializeComponent();

            Loaded += OnLoaded;
            Unloaded += OnUnloaded;
            AnimalsFlipView.SelectionChanged += AnimalsFlipViewOnSelectionChanged;
        }

        private object _ebuchieIndusy = new object();
        private bool _controlLoaded = false;

        private int _hackyfix = 0;

        //ВОПРОС можно ли без этого говна и что оно делает? вот тут
        // без этой жести никак? работает? не трогай... а то завоняет непонятная херь
        // непонятная херь - это контрол, который глючит. Из-за глюка прихордится это писать
        private void AnimalsFlipViewOnSelectionChanged(object sender, SelectionChangedEventArgs selectionChangedEventArgs)
        {
            lock (_ebuchieIndusy)
            {
                if (!_controlLoaded)
                    return;

                // запусти и перелистни один раз

                LevelViewModel viewModel = DataContext as LevelViewModel;
                if (viewModel != null)
                {
                    viewModel.SoundHandler();
                }

                return;
                //HACKYFIX: Daniel note:  Very hacky workaround for an api issue
                //Microsoft's api for getting item controls for the flipview item fail on the very first media selection change for some reason.  Basically we ignore the
                //first media selection changed event but spawn a thread to redo the ignored selection changed, hopefully allowing time for whatever is going on
                //with the api to get things sorted out so we can call the "ContainerFromItem" function and actually get the control we need

                // запусти. ничего не делай, листани, когда я выйду из дебага
                //то что серым цветом это не задействовано?
               
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            lock (_ebuchieIndusy)
            {
                // инициализируем медиаменеджер только когда загрузится вся страница (Loaded событие)
                MediaManager.Instance.Initialize();

                // тупик какой то -_- всё из-за контрола.

                LevelViewModel levelViewModel = DataContext as LevelViewModel;
                if (levelViewModel != null)
                {
                    levelViewModel.SoundHandler();
                }

                _controlLoaded = true;
            }
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            // отписываемся от событий, когда покидаем страницу, чтобы не жралась память

            //пашет. че нельзя было оставить как было?
            // а я не помню как было ща покажу
            //ну тоесть этот код был в App и не было вьюбейса
            // ну тут ты во вьюмодели решаешь что будет когда назат перейдёшь.
            // и мввмкросс кроме того чтобы назад перейти вроде ещё и стек чистит. 
            // короче как ща у тебя будет работать и когда ты на странице  кнопку назад сделаешь
            // ит когда просто нажмешь на телефоне кнопку назад аппаратную - логика обработки будет одна
            try
            {
                Loaded -= OnLoaded;
                Unloaded -= OnUnloaded;
                AnimalsFlipView.SelectionChanged -= AnimalsFlipViewOnSelectionChanged;

                // при выходе отсюда останавливаем все звуки
                MediaManager.Instance.StopSound();
            }
            catch
            {
                //ignored
            }
        }
    }
}

