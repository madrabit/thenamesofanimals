﻿
using System;
using System.Diagnostics;
using TheNamesOfAnimals.Managers;
using TheNamesOfAnimals.ViewModels;
using Windows.UI.Xaml;

namespace TheNamesOfAnimals.WinPhone.Views
{
    public sealed partial class AlbumView
    {
        public AlbumView()
        {
            InitializeComponent();
            Loaded += OnLoaded;
            Unloaded += OnUnloaded;
        }
        private object _ebuchieIndusy = new object();
        private bool _controlLoaded = false;

       

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            lock (_ebuchieIndusy)
            {
                MediaManager.Instance.Initialize();

               AlbumViewModel albumViewModel= DataContext as AlbumViewModel;
                if (albumViewModel != null)
                {
                    albumViewModel.MusicHandler();
                }

                _controlLoaded = true;
            }
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            // отписываемся от событий, когда покидаем страницу, чтобы не жралась память

            //пашет. че нельзя было оставить как было?
            // а я не помню как было ща покажу
            //ну тоесть этот код был в App и не было вьюбейса
            // ну тут ты во вьюмодели решаешь что будет когда назат перейдёшь.
            // и мввмкросс кроме того чтобы назад перейти вроде ещё и стек чистит. 
            // короче как ща у тебя будет работать и когда ты на странице  кнопку назад сделаешь
            // ит когда просто нажмешь на телефоне кнопку назад аппаратную - логика обработки будет одна
            try
            {
                Loaded -= OnLoaded;
                Unloaded -= OnUnloaded;
                MediaManager.Instance.StopMusic();
            }
            catch
            {
                //ignored
            }
        }
    }
}
