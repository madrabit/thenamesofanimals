﻿using System;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Cirrious.MvvmCross.WindowsCommon.Views;
using TheNamesOfAnimals.ViewModels;




namespace TheNamesOfAnimals.WinPhone.Views

{

  

    public class ViewBase : MvxWindowsPage
    {

        public ViewBase()
        {
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            Unloaded += OnUnloaded;
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            try
            {
                Unloaded -= OnUnloaded;
                HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
            }
            catch (Exception)
            {
                // ignoreed
            }
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            // вызывается, когда ты нажал аппаратную кнопку "назад"
            // если e.Handled = true - ты говоришь, что ты сам решишь чё делать
            // e.Handled = false - ты позволяешь системе самой перейти назад
            try
            {
                Frame rootFrame = Window.Current.Content as Frame;

                if (rootFrame != null && rootFrame.CanGoBack)
                {
                    var vm = ViewModel as ViewModelBase;
                    if (vm != null)
                    {
                        // говорим, точ нам назад не надо, а мы будем делать как хотим
                        e.Handled = true; // вот это че делает не пойму
                        // а тут мы как раз делаем как хотим, передаём управление команде "назад"
                        // ща тебя еще кое-че спрошу
                        vm.GoBackCommand.Execute(null);
                    }
                }
            }
            catch (Exception)
            {
                if (e != null)
                {
                    e.Handled = false;
                }
            }
        }
    }
}
