﻿using System;
using Windows.System;
using Windows.UI.Popups;
using TheNamesOfAnimals.Managers;
using TheNamesOfAnimals.WinPhone.Managers;

namespace TheNamesOfAnimals.WinPhone
{
    public class WpApp : CoreApp
    {
        protected override MediaManager GetMediaManager()
        {
            return new WpMediaManager();
        }

        protected override StoreManager GetStoreManager()
        {
            return new WpStoreManager();
        }

        protected override SettingsManager GetSettingsManager()
        {
            return new WpSettingsManager();
        }

        protected override LanguageManager GetLanguageManager()
        {
            return new WpLanguageManager();
        }

      }
}
