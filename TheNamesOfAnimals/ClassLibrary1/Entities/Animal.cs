﻿using Cirrious.MvvmCross.ViewModels;

namespace TheNamesOfAnimals.Entities
{

    public class Animal : MvxNotifyPropertyChanged // это короче базовый класс для объектов, свойства которых могут меняться
    {
       // давай короче лучше сделаем на всякий случай observable , чтобы в случае его не ебать мозг.ок
        public string Id { get; set; }

        public Animal(string id)
        {
            Id = id;
        }
    }
}