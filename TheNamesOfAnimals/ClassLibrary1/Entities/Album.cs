﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Cirrious.MvvmCross.ViewModels;

namespace TheNamesOfAnimals.Entities
{

    public class Album : MvxNotifyPropertyChanged
    {
        private ObservableCollection<Animal> _animals;
        public string Id { get; set; } // Id неизменное, поэтому не трогаем его

        public Album(string id)
        {
            Id = id;
        }

        public Album(string id, bool isFree)
        {
            Id = id;
            IsFree = isFree;
        }

        // слабо тут свойство ввести булево платный/неплатный? оно где то введено
        // у тебя только один бесплатный? да. тогда по умолчанию будет фолс ща
        //ты где ГДЕ ЖЕ ТЫ ГДЕ нанан на нанаааа я тут

        // бесплатный ли альбом (по умолчанию да)
        public bool IsFree { get; set; } = false;

        // т.к. логически в альбоме у тебя хранятся животные, то надо их сюджа добавить!
        public ObservableCollection<Animal> Animals
        {
            get { return _animals; }
            set { _animals = value; RaisePropertyChanged(); } // не забывай про raise, иначе хуй че обновится во вьюхе
        }

        // и в нём есть животные ыыы

        // меняяем лист на ObservableCollection и при выставлении нового значения оповещаем
        // вьюху об изменениях с помощью метода базового класса MvxNotifyPropertyChanged

        //public ObservableCollection<Animal> Animals { get; set; } // автосвойство не катит. т.к. на
        //// set надо доп. логику сделать, поэтому
        //public ObservableCollection<Animal> Animals
        //{
        //    get { return _animals; }
        //    set { _animals = value; RaisePropertyChanged(); } // теперь мы можем при выставлении значения доп. логику вкрючить
    } //alt + enter вызывает решарпер
        // теперь если ты изменить Animals во вьюмодели, то view ВСЕГДА сразу применит эти изменения,
        // т.к. у тебя 1. ObservableCollection дёргает RaisePropertyChanged при изменении любого элемента коллекции
        // 2.  set { _animals = value; RaisePropertyChanged();  при изменении всей коллекции также вью узнает об этом изменении
        // короче для изменяемых списков, если делать эти две вещи, то у тебя никогда не будет проблем с обновлением на вьюхе
    
}