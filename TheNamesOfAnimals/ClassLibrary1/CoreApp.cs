using System;
using Cirrious.CrossCore;
using Cirrious.CrossCore.IoC;
using TheNamesOfAnimals.Managers;

namespace TheNamesOfAnimals
{
    public abstract class CoreApp : Cirrious.MvvmCross.ViewModels.MvxApplication
    {
        public override void Initialize()


        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();


           
            // ��� ��� �� ������������ ��� ������ ���� �����
            //var albumManager = new AlbumManager();

            // �������������� AlbumManager
            Mvx.RegisterSingleton<AlbumManager>(new AlbumManager());

            // �������������� �������� ������ � ������
            Mvx.RegisterSingleton<MediaManager>(GetMediaManager());

            Mvx.RegisterSingleton<StoreManager>(GetStoreManager());

            Mvx.RegisterSingleton<SettingsManager>(GetSettingsManager());

            Mvx.RegisterSingleton<LanguageManager>(GetLanguageManager());



            // ��� ������� ���. � � ����� ������� ������� ��� ����.

            // Mvx.RegisterSingleton - ���� ������ �� ����������,
            // ������� ������ ����� ������ � ������ ��� � ����,
            // � �� ������ � ����� ������ ���� ������ ��������,
            // � ������ ������, ��� ��� ������������ ���������. 

            // �������� ��� ����� ����� �� ����� API ���������:
            //AlbumManager albumManager = Mvx.Resolve<AlbumManager>(); //���? ��



            RegisterAppStart<ViewModels.MenuViewModel>();
        }

        protected abstract MediaManager GetMediaManager();
        protected abstract StoreManager GetStoreManager();
        protected abstract SettingsManager GetSettingsManager();
        protected abstract LanguageManager GetLanguageManager();
        // � ������ ��� ��� ��� ���������� � ����� �� ����� ��������� -_-
        // ������ ��� ��� ���� ���������������� ������������� � ������� �������.
        // � � ��� ��� ������� � �� ������� ���. ������� �� ������� ���� ����� �����������,
        // � ����������� �������������� � �� �������, �.�. � WpApp �� �� �� ������ ���� � � �� ��� �� �����? ������ �����
    }
}