﻿using Cirrious.CrossCore;
using TheNamesOfAnimals.Entities;

namespace TheNamesOfAnimals.Managers
{
    public abstract class SettingsManager
    {
        public static SettingsManager Instance => Mvx.Resolve<SettingsManager>();

        public const string CounterInitKey = "Init_Counter";
        public const string isRantedKey = "IsRaned";

        public abstract void SaveInt(string key, int value);

        public abstract int? GetInt(string key);

        public abstract void SaveBool(string key, bool value);

        public abstract bool? GetBool(string key);

        public abstract void Remover();

        public abstract double Resolution();

       
    }
}
