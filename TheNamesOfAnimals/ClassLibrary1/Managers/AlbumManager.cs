﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Cirrious.CrossCore;
using TheNamesOfAnimals.Entities;
using TheNamesOfAnimals.Resources;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Resources;



namespace TheNamesOfAnimals.Managers
{

    public class AlbumManager
    {

        public static ObservableCollection<Album> Albums { get; }

       // public static ObservableCollection<Animal> Animals { get; }


        // вот должен был ты сначала пустой вот этот метод создать
        public Album GetAlbum (string albumId)
        {
            // у менеджера уже есть все альбомы, ему надо просто найти альбом по указанному ид
            // это LINQ версия
            //return Albums.First(album => album.Id == albumId);


            // сложно было додуматься до этого метода ? чет не додумался до форыча
            // да тут не в форыче дело. у тебя тут в менеджере уже всё есть что надо
            // ты его просишь что-то делать а потому думаешь что должен сам менеджер сделать.
            //

            Album result = null;

            // можно не LINQ
            foreach (Album album in Albums)
            {
                if (album.Id == albumId)
                {
                    // сохраняем найденный альбом в переменную, которую будем возвращать
                    result = album;
                    // прерываем foreach цикл, чтобы больше не искать ничего, т.к. уже нашли
                    break;
                }
            }

            // возврващаем найденный альбом
            return result; 
        }

        //Mvx.Resolve<тип твоего зареганного объекта>
        public static AlbumManager Instance
        {
            get { return Mvx.Resolve<AlbumManager>(); }
        }


        static AlbumManager()
        {


            // так коллекции инициализируются по-быстрому.
            Albums = new ObservableCollection<Album>(new List<Album>()
            {
                new Album("album_farm", true)
                {
                    Animals = new ObservableCollection<Animal>(new List<Animal>()
                    {
                        new Animal("dog"),
                        new Animal("cat"),
                        new Animal("guinea_pig"),
                        new Animal("Hamster"),
                        new Animal("bull"),
                        new Animal("chick"),
                        new Animal("chicken"),
                        new Animal("cock"),
                        new Animal("cow"),
                        new Animal("donkey"),
                        new Animal("duck"),
                        new Animal("goat"),
                        new Animal("goose"),
                        new Animal("horse"),
                        new Animal("lama"),
                        new Animal("pig"),
                        new Animal("pony"),
                        new Animal("rabbit"),
                        new Animal("sheep"),
                        new Animal("turckey")

                    })
                },
                
                
                new Album("album_forest")
                {
                    Animals = new ObservableCollection<Animal>(new List<Animal>()

                    {
                      new Animal("badger"),
                      new Animal("bear"),
                      new Animal("boar"),
                      new Animal("deer"),
                      new Animal("elk"),
                      new Animal("ferret"),
                      new Animal("fox"),
                      new Animal("frog"),
                      new Animal("hare"),
                      new Animal("hedgehog"),
                      new Animal("lynx"),
                      new Animal("mouse"),
                      new Animal("otter"),
                      new Animal("raccoon"),
                      new Animal("rat"),
                      new Animal("squirrel"),
                      new Animal("wolf"),
                      new Animal("shrew")


                    }
                    )
                },
                
                

                new Album("album_jungle")
                {
                    Animals = new ObservableCollection<Animal>(new List<Animal>()
                    {
                        new Animal("bat"),
                        new Animal("Capuchin_Monkey"),
                        new Animal("chimpanzee"),
                        new Animal("cougar"),
                        new Animal("crocodile"),
                        new Animal("Gorilla"),
                        new Animal("koala"),
                        new Animal("monkey"),
                        new Animal("panda"),
                        new Animal("tiger")

                    })
                },
                
                new Album("album_safari")
                {
                    Animals = new ObservableCollection<Animal>(new List<Animal>()
                    {
                        new Animal("Ant-eater"),
                        new Animal("Camel"),
                        new Animal("Cobra"),
                        new Animal("Elephant"),
                        new Animal("hippo"),
                        new Animal("Kangaroo"),
                        new Animal("Leopard"),
                        new Animal("lion"),
                        new Animal("Ostrich"),
                        new Animal("Rattlesnake"),
                        new Animal("Rhinoceros"),
                        new Animal("Zebra")

                    })
                },
                

                  new Album("album_birds")
                {
                    Animals = new ObservableCollection<Animal>(new List<Animal>()
                    {
                        new Animal("Bittern"),
                        new Animal("Blue_Amazon_Macaw"),
                        new Animal("Canary"),
                        new Animal("Corncrake"),
                        new Animal("Crow"),
                        new Animal("Cuckoo"),
                        new Animal("Dove"),
                        new Animal("Eagle"),
                        new Animal("Finch"),
                        new Animal("flamingo"),
                        new Animal("hawk"),
                        new Animal("hoopoe"),
                        new Animal("Kookaburra"),
                        new Animal("Lapwing"),
                        new Animal("Little_Grebe"),
                        new Animal("Macaw_parrot"),
                        new Animal("Magpie"),
                        new Animal("Meadow_Lark"),
                        new Animal("Mockingbird"),
                        new Animal("Nightingale"),
                        new Animal("Owl"),
                        new Animal("parrot"),
                        new Animal("Peacock"),
                        new Animal("Pelican"),
                        new Animal("Quail"),
                        new Animal("raven"),
                        new Animal("Red_Cardinal"),
                        new Animal("Red_lory"),
                        //new Animal("Red_parrot"),:'(
                        new Animal("Seagulls"),
                        new Animal("Swan"),
                        new Animal("tawny_frogmouth"),
                        new Animal("Toucan"),
                        new Animal("Vulture"),
                        new Animal("Whipperwhill"),
                        new Animal("Woodpecker")

                    })
                },
                 
                   new Album("album_sea")
                {
                    Animals = new ObservableCollection<Animal>(new List<Animal>()
                    {
                        new Animal("Dolphin"),
                        new Animal("killer_whale"),
                        new Animal("Penguin"),
                        new Animal("Sea_Elephant"),
                        new Animal("Sea_lion"),
                        new Animal("Walrus"),
                        new Animal("Whale"),
                        new Animal("white_dolhpin")

                    })
                },

                  


            new Album("album_insects")
                {
                    Animals = new ObservableCollection<Animal>(new List<Animal>()
                    {
                        new Animal("Bee"),
                        new Animal("beetle"),
                        new Animal("Bumblebee"),
                        new Animal("Cicada"),
                        new Animal("Cricket"),
                        new Animal("Fly"),
                        new Animal("grasshopper"),
                        new Animal("Mosquito")
                    })
                }


            });



        }



        public ObservableCollection<Album> GetAlbums()
        {
            return Albums;
        }


       
    }
}

/*
 // можно по долгому
            //var albums = new List<Album>(); // создсаём список альбомов
            //var dogAlbum = new Album("dog");
            //albums.Add(dogAlbum);
            //var catAlbum = new Album("cat");
            //albums.Add(catAlbum);

           
            //Albums = new List<Album>() // а тут пихаешь в статическое свойство класса
            //{
            //    new Album("frogs"),
            //    new Album("forest"),
            //    new Album("farm")
            //};

            //List<Album> albums = new List<Album>() // тут ты список пихаешь в локальную переменную метода 
            //{
            //    new Album("birds"),
            //    new Album("forest")
            //};

            //Albums = albums; // вот так. т.к. локальная переменная albums "пропадёт" потом

    // Метод возвращающий весь список "ресурсов"
        public static List<string> GetAllResourceStrings()
        {
            List<string> allRes = new List<string>();

            foreach (var oneRes in typeof(AppResources).GetProperties())
            {
                string res = Convert.ToString(oneRes.GetValue(oneRes));
                allRes.Add(res);

            }

            return allRes;
        }

     Albums = GetAlbumsFromResources();

    //Метод возвращающий все "альбомы" и "ресурсов"

private static ObservableCollection<Album> GetAlbumsFromResources()
        { 
            ObservableCollection<Album> result = new ObservableCollection<Album>();
            
            List<string> loadedRes = GetAllResourceStrings();
            foreach (string albumId in loadedRes)
            {
                
                if (albumId.Contains("album")) 
                {
                    Album test = new Album(albumId);
                    result.Add(test); 
                }

            }

            return result;
        }


*/
