﻿using Cirrious.CrossCore;

namespace TheNamesOfAnimals.Managers
{
    public abstract class LanguageManager
    {
        public static LanguageManager Instance => Mvx.Resolve<LanguageManager>();

        
        public abstract string Language { get; set; }
        
        public abstract void SaveString(string key, string value);

        public abstract string GetString(string key);

        public abstract string CheckLanguage();
    }
}