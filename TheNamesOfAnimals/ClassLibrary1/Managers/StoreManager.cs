﻿using System;
using System.Dynamic;
using Cirrious.CrossCore;

namespace TheNamesOfAnimals.Managers
{
    public abstract class StoreManager
    {
        // код внутриигрового продукта (как задано в панели разработчика)
        public const string FullVersionInAppCode = "TheNameOfAnimals2";


        public static StoreManager Instance => Mvx.Resolve<StoreManager>();

        //public abstract void Donate(object sender, EventArgs e);
        public abstract void Donate(); // это че ваще . кнопарт иди мол купи. я думал ты пожертвования решил собирать. не название из мануала не менял
        //вот оно ебать это свойство - купленная версия или нет. Я про свойтство альбома, а не игры
        public abstract bool FullVersion { get; }

        public abstract void RankMethod();

        public abstract void RankEvent();

        public abstract void PaymentReminder();

        public abstract void Vocabulary();
    }
}