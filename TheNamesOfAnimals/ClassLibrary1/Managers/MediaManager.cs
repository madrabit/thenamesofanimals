﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cirrious.CrossCore;

namespace TheNamesOfAnimals.Managers
{
    public abstract class MediaManager
    {
        public static MediaManager Instance => Mvx.Resolve<MediaManager>();

        // у тебя будет менеджер, которому ты будешь говорить что тебе надо.
        // например проиграть звук. или музыку типа MediaManager.Instance.PlaySound(Animal.Code) ща
        // неапример так ок mediaelement'а тут нет. поэтому надо унаследоваться от базового

        public abstract void Initialize();

        public abstract void PlayVoice(string code, Action callback = null);

        public abstract void PlaySound(string code, Action callback = null);

        public abstract void PlayAnswer(string code, Action callback = null);

        public abstract void StopSound();

        public abstract void PlayMusic(string code);

        public abstract void StopMusic();
    }
}
