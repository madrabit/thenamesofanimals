﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Cirrious.MvvmCross.ViewModels;
using TheNamesOfAnimals.Entities;
using TheNamesOfAnimals.Managers;


namespace TheNamesOfAnimals.ViewModels
{
    public class TaskViewModel: ViewModelBase
    {
        public bool _firstFlag { get; set; }
        public bool FirstFlag { get; set ;}
        public bool SecondFlag { get; set; }
        public bool ThirdFlag { get; set; }
        public bool FourthFlag { get; set; }

        public Animal SecondAnimal { get; set; }
        public Animal ThirdAnimal { get; set; }
        public Animal FourthAnimal { get; set; }

         private Animal _firstAnimal;
        public Animal FirstAnimal
        {
            get { return _firstAnimal; }
            set
            {

                if (_firstAnimal == value)
                    return;


                _firstAnimal = value; RaisePropertyChanged();
            }
        }

        int albumIndex = 0;

            
        public void RandChangeAnimals()
        {
            Random rnd = new Random();
            var linqResult = CurrentAlbum.Animals.OrderBy(animal => rnd.Next()).Take(4).ToList();

            AnimalsCollection = new ObservableCollection<Animal>(linqResult);

            

            FirstAnimal = AnimalsCollection[0];
            SecondAnimal = AnimalsCollection[1];
            ThirdAnimal = AnimalsCollection[2];
            FourthAnimal = AnimalsCollection[3];

            RightAnimalIndex = _rnd.Next(0, 3);
         


            FlagSetter(RightAnimalIndex);

            RaiseAllPropertiesChanged();


        }


        private Album _currentAlbum;

        public int AlbumLength { get; set; }

        public Album CurrentAlbum
        {
            get { return _currentAlbum; }
            set { _currentAlbum = value; RaisePropertyChanged(); }
        }

        private int animalIndex = 0;
        

        public ObservableCollection<Album> Albums { get; set; }



        // кнопки

        public MvxCommand<Animal> ChooseAnimalButton => new MvxCommand<Animal>(ChooseAnimalHandler);
        public Animal _ChooseAnimalButton123 { get; set; }

        public MvxCommand<Animal> ChooseAnimalButton123
        {
            get { return new MvxCommand<Animal>(ChooseAnimalHandler); }
        } 

        public void ChooseAnimalHandler(Animal obj)
            {
            
                _answer = true;
                PlayVoiceAnswer();

                RandChangeAnimals();
                PlayTaskSound();
                
            
                _answer = false;
                PlayVoiceAnswer();
           
        }

       

        public MvxCommand FirstButtonCommand
        {
            get { return new MvxCommand(FirstButtonHandler); }
        }
        public MvxCommand SecondButtonCommand
        {
            get { return new MvxCommand(SecondButtonHandler); }
        }
        public MvxCommand ThirdButtonCommand
        {
            get { return new MvxCommand(ThirdButtonHandler); }
        }
        public MvxCommand FourthButtonCommand
        {
            get { return new MvxCommand(FourthButtonHandler); }
        }
        public void FirstButtonHandler()
        {

            AllButtonsHandler(FirstFlag);
            
        }
        public void SecondButtonHandler()
        {

            AllButtonsHandler(SecondFlag);

        }
        public void ThirdButtonHandler()
        {

            AllButtonsHandler(ThirdFlag);

        }
        public void FourthButtonHandler()
        {

            AllButtonsHandler(FourthFlag);

        }

     

        public void AllButtonsHandler(bool flag)
        {
            if (flag == true)
            {
               
                    _answer = true;
                    PlayVoiceAnswer();
                

                FirstFlag = false;
                SecondFlag = false;
                ThirdFlag = false;
                FourthFlag = false;

                RandChangeAnimals();

                //тут баг

                
                PlayTaskSound2();


            }
            else
            {
                _answer = false;
                PlayVoiceAnswer();
            }
        }

        static Random _rnd = new Random();
        int RightAnimalIndex = _rnd.Next(0, 3);

        public void FlagSetter(int RndAnimal)

        {
            
                    

            switch (RndAnimal)
            {
                case 0:
                    FirstFlag = true;
                    break;
                case 1:
                    SecondFlag = true;
                    break;
                case 2:
                    ThirdFlag = true;
                    break;
                case 3:
                    FourthFlag = true;
                    break;
            }

            

        }

        public Animal _rightAnimal;
        public Animal RightAnimal
        {
            get { return _rightAnimal; }
            set
            {
                _rightAnimal = AnimalsCollection[RightAnimalIndex];
                RaisePropertyChanged();
            }
        }

        public bool _answer { get; set; }
        private string replyVoice;
        
        public void PlayVoiceAnswer()
        {
            
            if (_answer == true)
            {
                replyVoice = "correctly";

            }
            else
            {
                replyVoice = "mistake";
            }
            MediaManager.Instance.PlayAnswer(replyVoice);

            
        }


      

        public void PlayTaskSound2()
        {


            if (AnimalsCollection[RightAnimalIndex] != null)
            {
                MediaManager.Instance.PlayAnswer("correctly", () =>
                {


                    InvokeOnMainThread(
                        () => MediaManager.Instance.PlayVoice(AnimalsCollection[RightAnimalIndex].Id, () =>

                        {


                            InvokeOnMainThread(
                                () => MediaManager.Instance.PlaySound(AnimalsCollection[RightAnimalIndex].Id));
                        }));
                });

                

            }

        }

        public void PlayTaskSound()
        {
            
            
            if (AnimalsCollection[RightAnimalIndex] != null)
            {


                MediaManager.Instance.PlayVoice(AnimalsCollection[RightAnimalIndex].Id, () =>

                {


                    InvokeOnMainThread(() => MediaManager.Instance.PlaySound(AnimalsCollection[RightAnimalIndex].Id));
                });

            }

        }

        public ObservableCollection<Animal> AnimalsCollection { get; set; }

        public Random Rnd
        {
            get
            {
                return _rnd;
            }

            set
            {
                _rnd = value;
            }
        }

        public void Init()
        {
            Albums = AlbumManager.Instance.GetAlbums();


            
            CurrentAlbum = Albums[albumIndex];
            AlbumLength = CurrentAlbum.Animals.Count();

            
            Random rnd = new Random();
            IOrderedEnumerable<Animal> linqResult = CurrentAlbum.Animals.OrderBy(animal => rnd.Next());
            AnimalsCollection = new ObservableCollection<Animal>(linqResult);
            AnimalsCollection.Take(4);
            PlayTaskSound();
            FlagSetter(RightAnimalIndex);

            
           
            
            FirstAnimal = AnimalsCollection[0];
            SecondAnimal = AnimalsCollection[1];
            ThirdAnimal = AnimalsCollection[2];
            FourthAnimal = AnimalsCollection[3];



        }


    }
}
