using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using Cirrious.MvvmCross.ViewModels;
using System.Windows.Input;
using Cirrious.CrossCore.UI;
using TheNamesOfAnimals.Entities;
using TheNamesOfAnimals.Managers;


namespace TheNamesOfAnimals.ViewModels
{
    public class MenuViewModel
        : ViewModelBase
    {

        //--------Menu Buttons Binding Text----------


       
            
       
        
        //public string ChosenLanguage { get; set; } = LanguageManager.Instance.GetString("lang");

       
        
        public string _chosenLanguage { get; set; }
        
       public string ChosenLanguage {
           get
           {
               if (_chosenLanguage == null)
               {
                    _chosenLanguage = LanguageManager.Instance.CheckLanguage();
                   return _chosenLanguage;
               }
               else
               {
                    _chosenLanguage =  LanguageManager.Instance.GetString("lang");
                   return _chosenLanguage;
               }
           } 
           set { _chosenLanguage = value; } } 


        public string _langStartButton { get; set; } 



        public string StartButton
        {
            get
            {

                switch (ChosenLanguage)
                {
                    case "EN":
                        _langStartButton = "START GAME";
                        break;
                    case "FR":
                        _langStartButton = "DEMARRER JEU";
                        break;
                    case "IT":
                        _langStartButton = "INIZIA IL GIOCO";
                        break;
                    case "RU":
                        _langStartButton = "������ ����";
                        break;
                }

                return _langStartButton;
            }
            set { _langStartButton = value; RaisePropertyChanged(() => StartButton); }
        }

        private string _langTaskButton { get; set; }

        public string TaskButton
        {
            get
            {

                switch (ChosenLanguage)
                {
                    case "EN":
                        _langTaskButton = "GUESS ANIMAL";
                        break;
                    case "FR":
                        _langTaskButton = "GUESS ANIMALE";
                        break;
                    case "IT":
                        _langTaskButton = "GUESS ANIMALE";
                        break;
                    case "RU":
                        _langTaskButton = "������ ��������";
                        break;
                }

                return _langTaskButton;
            }
            set { _langTaskButton = value; RaisePropertyChanged(() => TaskButton); }
        }

        private string _langFullVersionButton { get; set; }

        public string FullVersionButton
        {
            get
            {

                switch (ChosenLanguage)
                {
                    case "EN":
                        _langFullVersionButton = "FULL VERSION";
                        break;
                    case "FR":
                        _langFullVersionButton = "VERSION COMPL�TE";
                        break;
                    case "IT":
                        _langFullVersionButton = "VERSIONE COMPLETA";
                        break;
                    case "RU":
                        _langFullVersionButton = "������ ������";
                        break;
                }

                return _langFullVersionButton;
            }
            set { _langFullVersionButton = value; RaisePropertyChanged(() => FullVersionButton); }
        }
        //--------------

        private bool _visibleStart = true;

        public bool VisibleStart
        {
            get
            {

                return _visibleStart;
            }
            set
            {
                if (_visibleStart != value && !StoreManager.Instance.FullVersion)
                {
                    _visibleStart = true;

                }
                
            }
        }

        private bool _visibleFull = false;

        public bool VisibleFull
        {
            get { return _visibleFull; }
            set
            {
                if (_visibleFull != value && StoreManager.Instance.FullVersion)
                    _visibleFull = StoreManager.Instance.FullVersion; 
            }
        }

        public MvxCommand GoPlayView
        {
            get { return new MvxCommand(GoPlayHandler); }
        }

        public void GoPlayHandler()
        {
            ShowViewModel<AlbumViewModel>();
        }

        //������ - ������ ��������

        public MvxCommand GoTaskView
        {
            get { return new MvxCommand(GoTaskHandler); }
        }

        public void GoTaskHandler()
        {
            ShowViewModel<TaskViewModel>();
        }

        //������ - ������ ������ 

        public MvxCommand GoFullVersionView
        {
            get { return new MvxCommand(GoFullVersionHandler); }
        }

        public void GoFullVersionHandler()
        {

            //�� ����� ��������� ������

              StoreManager.Instance.Donate();
            
        }

        //������ "���������"

        public MvxCommand GoOptionsView
        {
            get { return new MvxCommand(GoOptionsHandler); }
        }

        public void GoOptionsHandler()
        {
            ShowViewModel<OptionsViewModel>();
        }

        public static int _initCount = 0;

        public int InitCount
        {
            set { value = _initCount; RaisePropertyChanged(); }
            get { return _initCount; } 
        }
  
        public void Rate()
        {
            
            int? counterKey = SettingsManager.Instance.GetInt(SettingsManager.CounterInitKey);
            
            if (counterKey == null)
            {

               SettingsManager.Instance.SaveInt(SettingsManager.CounterInitKey, 1 );
               counterKey = SettingsManager.Instance.GetInt(SettingsManager.CounterInitKey);

                //Debug.WriteLine("her value: {0}", counterKey == null ? "UNDEFINED" : counterKey.Value.ToString());

                //Debug.WriteLine("net hera");
                Debug.WriteLine("counterValue = {0}", counterKey.Value);




            }
            else if(counterKey == 3)
            {
                StoreManager.Instance.RankEvent();
                Debug.WriteLine("counterValue = {0}", counterKey.Value);


            }
            else
            {
                SettingsManager.Instance.SaveInt(SettingsManager.CounterInitKey, counterKey.Value + 1);
                Debug.WriteLine("counterValue = {0}", counterKey.Value);
                return;
            }

            


        }


        public void Init()
        {
            StoreManager.Instance.Vocabulary();

            // SettingsManager.Instance.Remover();
            bool? isRated = SettingsManager.Instance.GetBool(SettingsManager.isRantedKey);
            if (isRated == null || isRated == false)
            {
                Rate();
            }



            LanguageManager.Instance.CheckLanguage();



            Debug.WriteLine("��� ����� " +  LanguageManager.Instance.Language);

            
            

        }
    }
}









        //private string _hello = "Hello MvvmCross";
        //      public string Hello
        //{ 
        //	get { return _hello; }
        //	set { _hello = value; RaisePropertyChanged(() => Hello); }
        //}



        //      public MvxCommand GoSecondView
        //      {
        //          get { return new MvxCommand(NavHandler); }
        //      }

        //      public void NavHandler()
        //      {
        //          ShowViewModel<SecondViewModel>();
        //      }
