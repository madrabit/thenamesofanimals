﻿using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;


namespace TheNamesOfAnimals.ViewModels
{
    public class SecondViewModel : ViewModelBase
    {
        private int _myWeight = 0;

        public int KettlebellWeight
        {
            
            get { return _myWeight; }
            set { _myWeight = value; RaisePropertyChanged(() => KettlebellWeight); }
        }



        private int _counter;

        public int PushUpCounter
        {

            get { return _counter; }
            set { _counter = value; RaisePropertyChanged(() => PushUpCounter); }
        }


        public MvxCommand PushUp
        {
            get { return new MvxCommand(PushUpHandler); }
        }
        public void PushUpHandler()
        {
            this._counter = this._counter + 1;
            RaisePropertyChanged(() => PushUpCounter);
            RaisePropertyChanged(() => SumWeight);
        }

        private int _sum;

        public int SumWeight
        {
            get { return _sum= _counter * _myWeight; }
           
        }

    }

}