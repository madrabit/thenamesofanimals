﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using TheNamesOfAnimals.Entities;
using TheNamesOfAnimals.Managers;

using System.Windows.Input;


namespace TheNamesOfAnimals.ViewModels
{
    // не забывай паблик везде писать
    public class AlbumViewModel : ViewModelBase
    {
        private Album _currentAlbum;
        public Album CurrentAlbum
        {
            get { return _currentAlbum; }
            set { _currentAlbum = value; RaisePropertyChanged(); } // это чтобы каждый раз при изменении свойства оно обновлялось во вьюхе
        } 

        // это мы из автосвойства сделали не автосвойство.


        

        public ObservableCollection<Album> Albums { get; set; } //лист или ovservablecollection 

        // Вариант инициализации 1
        public MvxCommand TestCommand => new MvxCommand(TestCommandMethod);
        private void TestCommandMethod()
        {
            // обработчик команды
        }

        // вариант инициализации 2 (более короткий) ок
        public MvxCommand TestCommand2 => new MvxCommand(() =>
        {
            // обработчик команды
        });

        //public  MvxCommand ForwardCommand => new MvxCommand(ForwardCommandMethod);

        //private void ForwardCommandMethod()
        //{
        //    CurrentAlbum = Albums.ElementAt(1);
        //} куда смотреть? картинки адовы xDDD да. Ты это. У меня ща подписка блять. Есть. На шатерсток картинки
        // можешь оттуда выбрать че хочешь -  я скачаю.да я думал пока все собрать. 
        private static int albumIndex = 0;

        public int AlbumCount
        {
            get { return Albums.Count(); }
        }


        public MvxCommand ForwardCommand
        {
            get { return new MvxCommand(ForwardCommandMethod); }
        }


        public void ForwardCommandMethod()
        {
            // если текущий альбом - не последний
            if (albumIndex != AlbumCount - 1) // как-то через жопу же написано было, наоборот. нет? чВ
            {



                //// или
                //albumIndex++;
                //CurrentAlbum = Albums[albumIndex]; // лишняя строка, но выглядит понятнее для чтения . вот это ElementAt тупо нp[[] меняется так ?
                //// ну как хочешь, так и пиши. [] меньше писанины. List поддерживает и [] и ElementAt
                //// а ваще листание так делается? или там другая логика
                //// какое листание? альбомов. Да как напишешь так и сделается. Так норм.ухаххаах!

                albumIndex = albumIndex + 1;
                CurrentAlbum = Albums[albumIndex];
                //CurrentAlbum = Albums.ElementAt(++albumIndex);

                //int x = 0;
                //int y = 0;

                //y = x++; // сначала присваивает x = 0 к y, потом прибавляет единицу к x
                //y = ++x; // сначала прибавляет единицу к x, потом присваивает что получилось к y
                //// это можно запомнить, но трудно читать всегда если не надрочен на это.
            }
            // если текущий альбом - последний
            else
            {
                // выбираем первый альбом
                albumIndex = 0;
                CurrentAlbum = Albums.ElementAt(albumIndex);
            }

            ButtonMusicHandler();

        }

        public void ButtonMusicHandler()
        {
            MediaManager.Instance.StopMusic();
            MediaManager.Instance.PlayMusic(CurrentAlbum.Id);
        }
        public void MusicHandler()
        {
            if (CurrentAlbum != null)
            {
                MediaManager.Instance.PlayMusic(CurrentAlbum.Id);
            }

        }
        public MvxCommand PlayCommand => new MvxCommand(MusicHandler);
        public MvxCommand BackCommand => new MvxCommand(() =>
        {
            // если текущий альбом - не первый
            if (albumIndex != 0)
            {
                CurrentAlbum = Albums.ElementAt(--albumIndex);
            }
            // если текущий альбом - первый
            else
            {
                // выбираем последний альбом
                albumIndex = AlbumCount - 1;
                CurrentAlbum = Albums.ElementAt(albumIndex);
            }

            ButtonMusicHandler();
        });

        public MvxCommand GoToLevelView
        {
            get { return new MvxCommand(GoToLevelHandler); }
        }

        //public bool FullVersion { get; set; }

            // а вот тут проверяется типо отдавать оплаченые или нахуй послать иииии еще чет было забыл где
            //блять забыл  ну пока вот. где ещё?
        public void GoToLevelHandler()
        {
            // че за хуйня. ну типо только первый альбом бесплатный
            // это говно код

            // если альбом бесплатный или если у нас куплена полная версия, то показыаем альбом
            if (CurrentAlbum.IsFree || StoreManager.Instance.FullVersion)
            {
                ShowViewModel<LevelViewModel>(new { albumId = CurrentAlbum.Id });
            }
            // если альбом платный и не оплачен - пинаем на оплату
            else
            {
                StoreManager.Instance.PaymentReminder();
            }

            //if (CurrentAlbum.Id == "farm")
            //{
            //    ShowViewModel<LevelViewModel>(new { albumId = CurrentAlbum.Id });
            //}

            //else if (CurrentAlbum.Id != "farm" && StoreManager.Instance.FullVersion == false)
            //{
            //    //пнуть на оплату
            //}
            //else
            //{
            //    ShowViewModel<LevelViewModel>(new { albumId = CurrentAlbum.Id });
            //}
        }

        public void Init()
        {
            //AlbumManager albumManager = Mvx.Resolve<AlbumManager>();
            //Albums = albumManager.GetAlbums(); 
            

            Albums = AlbumManager.Instance.GetAlbums();//Oo
                                                       // это чтобы не писать каждый раз AlbumManager albumManager = Mvx.Resolve<AlbumManager>();
                                                       // kakoy u tebya pervyj albom pokazyvaetsya ?
                                                       //ферма. а при переходе назад тормозит пикча заставки. мульятшная такая
                                                       // sd omikom? угу
                                                       //ваще не тупит u tebya kakoy telefon?920 люмиа норм. но при меньше разрешении было пёзже
                                                       //так  че все фотки резать? а если у типов буд
                                                       // dobro pozhalovat v gemor s raznymi telefonami! бля xDDDDDDDDDDDDDDDDDDDD!!!!!! LUL
                                                       // nu ya x3. tut ili delat menshee kartinki i smotret kak oni rastyanutsya esli ekran bolshe.
                                                       // nu ndao eshe provesti analiz kakie vashe razresheniya na windows phonax. maximalnoe i minimalnoe
                                                       // i otsuda plyasat. 1980 вроде самое большое. в игре кстати не тормозит. сделать бы для менюхи и альбомов
                                                       // u menya v androide bylo. zasunul nu blyad na 25% mozhet bolshe kartinku 4em ekran - on suka vyletaet ot etogo
                                                       // днярство. а ты показывал какую то херь. что оно определяет устройство и нужную пикчу подставляет. nu tam ot ppi
                                                       // zavisit vrode, shya
                                                       // ты тут? da хм
                                                       //ну и че делать бум? uninstall это толлько в лоле помогает -_- ne znayu.
                                                       //ну похуй. короче ты это поясни как разбить терь приложуху на платную и бесплатную части
                                                       // po povodu kartinok nado guglit' tipa resolutionscale ili kak to tak. ya po4ital tam pizdec kakoy to.
                                                       //
                                                       // po povodu platnoy i besplantnoyy.
                                                       // est' dva tipa prodazh v WP: consumable i durable. consumable - rasxoduemoe (tipa monetki),
                                                       // durable - postoyannoe (naprimer, u tebya eto kakoyto nedostupnyj albom)
                                                       // vot tebe надо определить какие части твоег оприложения будут платными.
                                                       // отсюда создать нужные покупки в панели разработчика (например "альбом ХХХ").
                                                       // + тебе надо будет написать логику по покупке внутри приложения и правильному отображению
                                                       // платного альбома (типа видмый если он куплен, или недоступный с кнропкой купить если не куплен)
                                                       // а есть мануал на эту ебань?
                                                       // https://code.msdn.microsoft.com/windowsapps/Licensing-API-Sample-19712f1a
                                                       // "windows phone in app purchase tutorial " <- google
                                                       // "https://habrahabr.ru/sandbox/82947/"
                                                       // https://habrahabr.ru/post/226777/
                                                       // pravilno tam na habre napisano ili net - ne ru4aus' esli 4e. eshe 4et?
                                                       // да не я взботну вопрос, прежде чем че-то спрашивать
            CurrentAlbum = Albums[albumIndex];
        }
    }
}
