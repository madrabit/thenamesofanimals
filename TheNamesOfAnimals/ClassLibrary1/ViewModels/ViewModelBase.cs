﻿using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using TheNamesOfAnimals.Resources;


namespace TheNamesOfAnimals.ViewModels
{
    public abstract class ViewModelBase : MvxViewModel
    {
        private static AppResources Resources => new AppResources();

        public AppResources Locale { get { return Resources; } }

        public ICommand GoBackCommand
        {
            get
            {
                return new MvxCommand(() => Close(this));
            }
        }
    }
}
