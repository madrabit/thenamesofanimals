﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using TheNamesOfAnimals.Entities;
using TheNamesOfAnimals.Managers;
using System.Windows.Input;
using System.Windows;
using static TheNamesOfAnimals.Managers.SettingsManager;


namespace TheNamesOfAnimals.ViewModels
{
    public class OptionsViewModel : ViewModelBase
    {

        public string ChosenLanguage { get; set; } = LanguageManager.Instance.GetString("lang"); // для теста уже выбран язык
        public string Selection { get; set; } = "_selected";
        


        
        public Language LangaugeEN { get; set; }
        public Language LangaugeFR { get; set; }
        public Language LangaugeIT { get; set; }
        public Language LangaugeRU { get; set; }

        public void SelectLang(string lng)
        {
            LangaugeEN.Id = "en_lang_button";
            LangaugeFR.Id = "fr_lang_button";
            LangaugeIT.Id = "it_lang_button";
            LangaugeRU.Id = "ru_lang_button";

            switch (lng)
            {
                case "en":
                    LangaugeEN.Id = LangaugeEN.Id + Selection;
                break;
                case "fr":
                    LangaugeFR.Id = LangaugeFR.Id + Selection;
                    break;
                case "it":
                    LangaugeIT.Id = LangaugeIT.Id + Selection;
                    break;
                case "ru":
                    LangaugeRU.Id = LangaugeRU.Id + Selection;
                    break;
            }
            RaiseAllPropertiesChanged();
        }


        public MvxCommand EnButtonCommand
        {
            get { return new MvxCommand(SelectEnHandler); }
        }
        public void SelectEnHandler()
        {

            SelectLang("en");
            LanguageManager.Instance.SaveString("lang","en");

        }

        public MvxCommand FrButtonCommand
        {
            get { return new MvxCommand(SelectFrHandler); }
        }
        public void SelectFrHandler()
        {

            SelectLang("fr");
            LanguageManager.Instance.SaveString("lang", "fr");
        }

        public MvxCommand ItButtonCommand
        {
            get { return new MvxCommand(SelectItHandler); }
        }
        public void SelectItHandler()
        {

            SelectLang("it");
            LanguageManager.Instance.SaveString("lang", "it");


        }

        public MvxCommand RuButtonCommand
        {
            get { return new MvxCommand(SelectRuHandler); }
        }
        public void SelectRuHandler()
        {

            SelectLang("ru");
            LanguageManager.Instance.SaveString("lang", "ru");


        }


      


        public void Init()
        {
            LangaugeEN = new Language("en_lang_button");
            LangaugeFR = new Language("fr_lang_button");
            LangaugeIT = new Language("it_lang_button");
            LangaugeRU = new Language("ru_lang_button");
            SelectLang(ChosenLanguage);

        }

    }

   
}