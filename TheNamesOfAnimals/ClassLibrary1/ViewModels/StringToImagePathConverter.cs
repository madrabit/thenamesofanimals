﻿using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore.Converters;
using System;
using System.Globalization;



namespace TheNamesOfAnimals.ViewModels
{
    public class StringToImagePathConverter : IMvxValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string albumCode = value.ToString();

            return new BitmapImage(new Uri(albumCode));


        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    internal class BitmapImage
    {
        private Uri uri;

        public BitmapImage(Uri uri)
        {
            this.uri = uri;
        }
    }
}